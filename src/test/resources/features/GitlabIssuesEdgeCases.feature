@IntegrationTest
Feature: Edge Scenarios

  Scenario: To verify user should not be able to move the issue in the same project, i.e. source project to source project
    When User moved an issue within the same project
    Then User see target project missing error

  Scenario: To verify user should not be able to subscribe to an issue for which  user is already subscribe
    When User subscribes to an issue for which user is already subscribed
    Then Subscription operation is not successful

  Scenario: To verify user should not be able to unsubscribe to an issue for which user is already unsubscribe
    When User unsubscribe to an issue for which user is already unsubscribe
    Then Unsubscription operation is not successful

  Scenario: To verify user should not be able to create an issue without title
    When User create an issue without title
    Then User see the title is missing error

  Scenario: To verify user should not be able to todo for an issue which already has todo exist with it
    When User create a todo for the issue for which todo already exist
    Then User create todo operation is not successful