@IntegrationTest
Feature: CRUD operation issues API

  Scenario: User should be able to create a new issue and read the newly created issue
    Given User creates a new issue
    When User update an existing issue
    And User delete an existing issue
    Then User cannot see the deleted issue in the issue list