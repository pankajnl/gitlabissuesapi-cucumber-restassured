package com.issues.stepdefinition;

import com.issues.api.IssuesApi;
import com.issues.utils.Helper;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateReadUpdateDeleteSteps {
    private IssuesApi objIssueApi = new IssuesApi();
    private String updateString;
    private Helper objHelper = new Helper();

    @When("^User creates a new issue$")
    public void userCreatesNewIssue() {
        objIssueApi.createIssues();
        objHelper.validateResponseCode(201);
        objHelper.validateResponseBodyContainsValue("id");
        objHelper.validateResponseBodyContainsValue("iid");
    }

    @When("^User update an existing issue$")
    public void userUpdateExistingIssue() {
        updateString = objHelper.getAlphanumericString(8);
        objIssueApi.updateIssue(updateString);
        objHelper.validateResponseCode(200);
        objHelper.validatePropertyValueInResponseBody("title", updateString);
    }

    @When("^User delete an existing issue$")
    public void userDeleteExistingIssue() {
        objIssueApi.deleteIssue();
        objHelper.validateResponseCode(204);
    }

    @Then("^User cannot see the deleted issue in the issue list$")
    public void verifyExistingIssueDeleted() {
        objIssueApi.readIssues();
        objHelper.validateResponseCode(200);
        objHelper.validateResponseBodyDoesNotContainedValue(updateString);
    }

}
