package com.issues.stepdefinition;

import com.issues.api.IssuesApi;
import com.issues.utils.Helper;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GitlabIssuesEdgeCasesSteps {
    private IssuesApi objIssueApi = new IssuesApi();
    private Helper objHelper = new Helper();

    @When("^User moved an issue within the same project$")
    public void userMovedIssueWithinSameProject() {
        objIssueApi.moveIssue();
        objHelper.validateResponseCode(400);
    }

    @When("^User subscribes to an issue for which user is already subscribed$")
    public void userSubscribesIssueForWhichUserAlreadySubscribed() {
        objIssueApi.subscribeIssue();
    }

    @When("^User unsubscribe to an issue for which user is already unsubscribe$")
    public void userUnsubscribeIssueForWhichUserAlreadyUnsubscribe() {
        objIssueApi.unsubscribeIssue();
    }

    @When("^User create an issue without title$")
    public void userCreateIssueWithoutTitle() {
        objIssueApi.createIssueWithoutTitle();
        objHelper.validateResponseCode(400);
    }

    @When("^User create a todo for the issue for which todo already exist$")
    public void userCreateTodoForTheIssueForWhichTodoAlreadyExist() {
        objIssueApi.createTodoForIssue();
    }

    @Then("^User see target project missing error$")
    public void userSeeTargetProjectMissingError() {
        objHelper.validatePropertyValueInResponseBody("error", "to_project_id is missing");
    }

    @Then("^Subscription operation is not successful$")
    public void subscriptionOperationIsNotSuccessful() {
        objHelper.validateResponseCode(304);
    }

    @Then("^Unsubscription operation is not successful$")
    public void unsubscriptionOperationIsNotSuccessful() {
        objHelper.validateResponseCode(304);
    }

    @Then("^User see the title is missing error$")
    public void userSeeTheTitleIsMissingError() {
        objHelper.validatePropertyValueInResponseBody("error", "title is missing");
    }

    @Then("^User create todo operation is not successful$")
    public void userCreateTodoOperationIsNotSuccessful() {
        objHelper.validateResponseCode(304);
    }

}
