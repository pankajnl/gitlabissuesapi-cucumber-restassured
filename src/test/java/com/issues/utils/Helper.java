package com.issues.utils;

import com.issues.api.AuthorizationApi;
import gherkin.deps.com.google.gson.JsonObject;
import gherkin.deps.com.google.gson.JsonParser;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import org.junit.Assert;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;

public class Helper {
    private AuthorizationApi objauthapi = new AuthorizationApi();
    private static Response response;
    private Map<String, String> queryParameters = new HashMap<String, String>();
    private JsonPath jsonPath;
    private String accessToken;

    //Constructor will get call as soon as object of this class initiate, it will extract access token from authorization API and stored it in accessToken variable for further use
    public Helper() {
        if (accessToken == null)
            accessToken = objauthapi.getAccessToken();
    }

    //This is generic method to get the query parameters from api class and stored in the queryParameters hash map
    public void addQueryParameters(String key, String value) {
        this.queryParameters.put(key, value);
    }

    //This is the generic method to perform GET request for any api.
    public void performGetRequest(String apiUrl) {
        response = given().auth().preemptive().oauth2(accessToken)
                .when()
                .get(apiUrl);
    }

    //This is  generic method to validate the status code of Api.
    public void validateResponseCode(int statusCode) {
        Assert.assertEquals(statusCode, response.getStatusCode());
    }

    //This is generic method to perform the POST request
    public void performPostRequest(String apiUrl) {
        response = given().auth().preemptive().oauth2(accessToken)
                .queryParams(queryParameters)
                .when()
                .post(apiUrl);
    }

    //This is generic method to perform the PUT request
    public void performPutRequest(String apiUrl) {
        response = given().auth().preemptive().oauth2(accessToken)
                .queryParams(queryParameters)
                .when()
                .put(apiUrl);
    }

    //This is generic method to perform the DELETE request
    public void performDeleteRequest(String apiUrl) {
        response = given().auth().preemptive().oauth2(accessToken)
                .when()
                .delete(apiUrl);
    }

    //This is generic method used to check the value which is coming from step definition is present in the response body.
    public void validateResponseBodyContainsValue(String field) {
        jsonPath = response.jsonPath();
        Assert.assertNotNull(jsonPath.get(field));
    }

    //This is generic method used to check the value which is coming from step definition is not present in the response body.
    public void validateResponseBodyDoesNotContainedValue(String field) {
        ResponseBody body = response.getBody();
        String bodyAsString = body.asString();
        Assert.assertFalse(bodyAsString.contains(field));
    }

    //This is generic method used to validate the actual and expected value for value provided in step definition or api.
    public void validatePropertyValueInResponseBody(String property, String propertyValue) {
        response.then().body(property, containsString(propertyValue));
    }

    //This is generic method to get the property value of field
    public String getValueFromResponseBody(String propertyKey) {
        jsonPath = new JsonPath(response.asString());
        return jsonPath.getString(propertyKey);
    }

    //This method is used to provide data from json based on received request from step definition or api so that user does not have to hard code any data in api test
    public String getDataFromJson(String requiredData) {
        String jsonPath = "src\\test\\resources\\TestData.json";
        JsonParser jsonParser = new JsonParser();
        JsonObject jsonObject = null;
        try {
            jsonObject = jsonParser.parse(new FileReader(jsonPath)).getAsJsonObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String data = jsonObject.get(requiredData).getAsString();
        return data;

    }

    //This method is used to generate random alphanumeric string based on length provided by user.
    //Purpose to use random string is if user try to create issue with the same title then after few attempt api gives 400 bad request error
    public String getAlphanumericString(int stringLenght) {
        // chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        StringBuilder sb = new StringBuilder(stringLenght);

        for (int i = 0; i < stringLenght; i++) {
            int index = (int) (AlphaNumericString.length() * Math.random());
            sb.append(AlphaNumericString.charAt(index));
        }
        return sb.toString();
    }
}
