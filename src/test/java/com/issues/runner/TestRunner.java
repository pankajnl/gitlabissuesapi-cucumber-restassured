package com.issues.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

//Below configurations are used to run all the cucumber scenarios based on mentioned tags and stored the result under target folder
@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features",
        glue = {"com/issues/stepdefinition"},
        monochrome = true,
        plugin = {"pretty", "json:target/cucumber-reports/results.json", "junit:target/cucumber-reports/results.xml", "html:target/cucumber-reports"},
        tags = {"@IntegrationTest"}
)
public class TestRunner {
}
