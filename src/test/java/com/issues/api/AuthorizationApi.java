package com.issues.api;

import com.issues.utils.ConfigFileReader;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class AuthorizationApi {
    private ConfigFileReader objconfig = new ConfigFileReader();

    //Method is use to get access_token to authorize other issues api and credential flow used here to get OAuth2 token is Resource owner password
    public String getAccessToken() {
        Response response = given()
                .formParam("grant_type", "password")
                .formParam("username", objconfig.getUsername())
                .formParam("password", objconfig.getPassword())
                .when()
                .post(objconfig.getAuthenticationUrl());
        JsonPath jsonPathEvaluator = response.jsonPath();
        String accessToken = jsonPathEvaluator.getString("access_token");
        return accessToken;
    }
}
