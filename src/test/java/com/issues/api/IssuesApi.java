package com.issues.api;

import com.issues.utils.ConfigFileReader;
import com.issues.utils.Helper;

public class IssuesApi {
    private ConfigFileReader objconfig = new ConfigFileReader();
    private Helper objHelper = new Helper();

    //Method create new project issue with random alphanumeric title
    public void createIssues() {
        String createIssueUrl = objconfig.getBaseUrl() + "/projects/" + objconfig.getProjectId() + "/issues";
        objHelper.addQueryParameters("title", objHelper.getAlphanumericString(8));
        objHelper.performPostRequest(createIssueUrl);
    }

    //Method reads all the issues for which user has access
    public void readIssues() {
        String getIssueUrl = objconfig.getBaseUrl() + "/issues";
        objHelper.performGetRequest(getIssueUrl);
    }

    //Method edit existing issue based on internal id of a project's issue(iid) and we choose title to update the issue
    public void updateIssue(String updatedTitle) {
        String updateIssueUrl = objconfig.getBaseUrl() + "/projects/" + objconfig.getProjectId() + "/issues/" + objHelper.getValueFromResponseBody("iid");
        objHelper.addQueryParameters("title", updatedTitle);
        objHelper.performPutRequest(updateIssueUrl);
    }

    //Method delete existing issue based on internal id of a project's issue(iid)
    public void deleteIssue() {
        String deleteIssueUrl = objconfig.getBaseUrl() + "/projects/" + objconfig.getProjectId() + "/issues/" + objHelper.getValueFromResponseBody("iid");
        objHelper.performDeleteRequest(deleteIssueUrl);
    }

    //Method moves an issue to a different project based on internal id of a project's issue(iid) and the ID of the new project(to_project_id)
    public void moveIssue() {
        String moveIssueUrl = objconfig.getBaseUrl() + "/projects/" + objconfig.getProjectId() + "/issues/" + objHelper.getDataFromJson("moveIssueId") + "/move";
        objHelper.performPostRequest(moveIssueUrl);
    }

    //Method subscribe user to an issue to receive notification based on internal id of a project's issue(iid)
    public void subscribeIssue() {
        String subscribeIssueUrl = objconfig.getBaseUrl() + "/projects/" + objconfig.getProjectId() + "/issues/" + objHelper.getDataFromJson("subscribeIssueId") + "/subscribe";
        objHelper.performPostRequest(subscribeIssueUrl);
    }

    //Method unsubscribe user to an issue to not receive notification based on internal id of a project's issue(iid)
    public void unsubscribeIssue() {
        String unsubscribeIssueUrl = objconfig.getBaseUrl() + "/projects/" + objconfig.getProjectId() + "/issues/" + objHelper.getDataFromJson("unsubscribeIssueId") + "/unsubscribe";
        objHelper.performPostRequest(unsubscribeIssueUrl);
    }

    //Method tries to create issue without title which is mandatory field- unsuccessful operation
    public void createIssueWithoutTitle() {
        String createIssueUrlWithoutTitle = objconfig.getBaseUrl() + "/projects/" + objconfig.getProjectId() + "/issues";
        objHelper.performPostRequest(createIssueUrlWithoutTitle);
    }

    //Method creates an to-do for the user on an issue based on internal id of a project's issue(iid)
    public void createTodoForIssue() {
        String createTodoIssueUrl = objconfig.getBaseUrl() + "/projects/" + objconfig.getProjectId() + "/issues/" + objHelper.getDataFromJson("todoIssueId") + "/todo";
        objHelper.performPostRequest(createTodoIssueUrl);
    }
}
