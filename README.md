# GitLab API Cucumber-Rest assured

The Project contains automated GitLab project issues test scenarios written in BDD format. Scenarios are CRUD operation on issues API and few edge case scenarios

## Getting Started

To work with the project please check out the master branch and store the project on local machine. See Running the tests section to run all the scenarios.

### Prerequisites

Below tools should be installed on local machine

```
1) Idea like IntelliJ/Android studio/Eclipse
2) Gradle build tool
3) Java SE Development Kit(jdk)
```

### Installing

A step by step series of instruction to get the project working on your local machine

```
1) Launch the development idea like Intellij/Android Studio/Eclipse
2) Open the project from the location where it has been stored during checkout
```



## Running the tests

Test scenarios can be run in two different ways as explained below

```
1) Run the TestRunner java class to run all the scenarios
2) In the terminal or cmd travers till project path and run the command "gradle cucumber"
To run above command first command gradle path should be stored under environment variable please follow https://gradle.org/install/ for more reference.
```



## Results

Test results are stored under target/cucumber-reports folder in different formats like html, json, xml and file name for html report is  index



## Built With

- [REST-assured](http://rest-assured.io/) - Java library
- [Gradle](https://gradle.org/) - Build tool
- [Cucumber](https://cucumber.io/) - Tool to support BDD

## Authors

- **Pankaj Nalawade**